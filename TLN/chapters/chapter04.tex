%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHAPTER 4: FINITE AUTOMATA
\chapter{Finite automata}
\localtableofcontents

\newpage


\section{Deterministic finite automata}

\begin{definition}{Deterministic finite automaton \translation{autómata finito determinista}}

A deterministic finite automaton (DFA) is a 5-tuple $(K, \Sigma, \delta,s, F)$, where
\begin{enumerate}[label=]
\item $K$ is a non-empty set of states
\item $\Sigma$ is an alphabet
\item $s \in K$ is the initial state
\item $F \subseteq K$ is a set of final states
\item $\delta: K \times \Sigma \to K$ is the transition function 
\end{enumerate}
\end{definition}  

\newpage


\begin{definition}{Configuration of a DFA \translation{configuración}}

  A configuration of $(K, \Sigma, \delta,s, F)$ is any $(q, w) \in K \times \Sigma^*$.
  \begin{remark}
    $q$ represents the current state of the DFA, and $w$ is the string that is still to be processed by the DFA.
  \end{remark}

\end{definition}  


\begin{definition}{Initial configuration of a DFA}

An initial configuration of $(K, \Sigma,\delta,s, F)$ is any pair $(s, w)$, with $w \in \Sigma^*$.
\end{definition}  


\begin{definition}{Final configuration of a DFA}

A final configuration of $(K, \Sigma,\delta, s,F)$ is any pair $(q, \varepsilon)$, with $q \in K$.
\end{definition}  

\newpage


\begin{definition}{Direct transition of a DFA \translation{transición directa}}

$(K, \Sigma,\delta, s, F)$ performs a direct transition from configuration $(q, w)$ to $(q', w')$, noted $(q, w) \vdash (q', w')$, iff
\begin{center}
$\exists \sigma \in \Sigma \suchthat (w=\sigma w') \land (\delta(q, \sigma)=q')$
\end{center}

\begin{remark}
A direct transition of a DFA is the function
\begin{center}
$\begin{aligned}
\vdash \colon K \times \Sigma^+ & \to K \times \Sigma^* \\
                  (q,\sigma w') & \to (\delta(q,\sigma),w')
\end{aligned}$
\end{center}
\end{remark}
\end{definition}  


\begin{definition}{Transiting in $n$ steps \translation{transitar}}

Let $(q,w)$ and $(q',w')$  be configurations of a DFA.

We say that $(q,w)$ transits in $n$ steps to $(q',w')$, noted $(q,w) \vdash ^n (q',w')$,\\
with $n>1$, iff $\exists ~C_1, C_2, \ldots , C_{n-1}$ configurations of that DFA such that
\begin{center}
  $(q,w) \vdash C_1,~C_1 \vdash C_2 ,~\ldots,~C_{n-1} \vdash (q',w')$
\end{center}

We say that $(q,w)$ transits in $1$ step to $(q',w')$, noted $(q,w) \vdash ^1 (q',w')$, iff
\begin{center}
  $(q,w)\vdash (q',w')$
\end{center}

We say that $(q,w)$ transits in $0$ step to $(q',w')$, noted $(q,w) \vdash ^0 (q',w')$, iff
\begin{center}
  $(q,w)= (q',w')$
\end{center}
\end{definition}


\begin{definition}{Transiting in at least one step}

Let $(q,w)$ and $(q',w')$ be configurations of a DFA.

\smallskip
We say that $(q,w)$ transits in at least one step to $(q',w')$, \\
noted $(q,w) \vdash ^+ (q',w')$, iff
\begin{center}
$\exists n>0\suchthat(q,w) \vdash ^n (q',w')$
\end{center}
\end{definition}


\begin{definition}{Transiting}

Let $(q,w)$ and $(q',w')$ be configurations of a DFA.

We say that $(q,w)$ transits to $(q',w')$, noted $(q,w) \vdash^* (q',w')$, iff
\begin{center}
  $\exists n\geq 0\suchthat(q,w) \vdash ^n (q',w')$
\end{center}
\end{definition}


\begin{definition}{Computation. Length of a computation}

	Let $M$ be a DFA, a computation of $M$ is any sequence of configurations $C_0,C_1,C_2,...,C_n$ such that $C_i\vdash C_{i+1}$ $\forall i\in\{0,...,n-1\}$ with $n\geq0$.\\
	It will be noted as $C_0\vdash C_1\vdash C_2\vdash...\vdash C_n$, and the length of the computation is $n$.
\end{definition}


\begin{definition}{Well started/terminated/complete computation}

A well started computation is any computation in which its first configuration is an initial configuration. \\
A terminated computation is any computation in which its last configuration is a final configuration. \\
A computation is complete if it is well started and terminated.
\end{definition}


\begin{definition}{Inaccessible state}

$ q \in K \text{ is an inaccessible state of } (K, \Sigma, s, F, \delta) \; \text{ iff } \; \nexists x \in \Sigma^* \suchthat (s, x) \vdash^* (q, \varepsilon) $
\end{definition}


\begin{definition}{Accepted string}

Let $M=(K,\Sigma,\delta,s,F)$ and $w \in \Sigma^*$.
$w$ is accepted by $M$ iff
\begin{center}
$\exists q\in F \suchthat (s,w)\vdash^*(q,\emptystring)$.
\end{center}
\end{definition}


\begin{definition}{Accepted language}

\begin{center}
$\Lb(M)=\{w\in\Sigma^* \suchthat w \text{ is accepted by } M\}$
\end{center}
\end{definition}


\begin{definition}{Set of languages accepted by DFA}

The set of the languajes accepted by DFA, denoted as $\La(DFA)$, is defined as:
\begin{center}
$\La(DFA)=\{L \suchthat \exists M \text{ DFA}, \Lb(M)=L\}$
\end{center}
\end{definition}


\begin{definition}{States diagram}

The state diagram of a DFA is a directed graph in which each node represents a state, and each edge represents a transition for a certain input between the states the edge conects (i.e. if $\delta(q,\sigma)=q'$ then there will be an edge from $q$ to $q'$ labeled as $\sigma$).\\
Final states are represented with double circles and the initial state is indicated with "$>$".\\
\end{definition}

\newpage


\begin{example}
Let $M=(\{q_0,q_1,q_2\}, \{a,b\}, \delta, q_0, \{q_2\})$ be a DFA with:\\

\begin{table}[h!]
\begin{tabular}{c|c|c}
  $\delta(q,\sigma)$ & $a$ & $b$\\
  \hline
  $q_0$& $q_0$ & $q_1$\\
  \hline
  $q_1$& $q_2$ & $q_1$\\
  \hline
  $q_2$& $q_2$ & $q_0$
\end{tabular}
\end{table}

\begin{center}
  \begin{tikzpicture}[auto,
      shorten > = 1pt, 
  node distance = 4cm and 3cm]
  \node[state] (q0) {$q_0$};
  \node[inner sep=0pt,outer sep=-1pt,left=0pt of q0.west]{$>$};
  \node[state] (q1) [right of=q0] {$q_1$};
  \node[state,accepting] (q2) [below of=q1] {$q_2$};
  \path[->] (q0) edge [loop above] node {$a$} ()
                 edge ["$b$"] (q1)
            (q1) edge ["$a$"] (q2)
                 edge [loop above, "$b$"] ()
            (q2) edge ["$b$"] (q0)
                 edge [loop right, "$a$"] ();
  \end{tikzpicture}
\end{center}
\end{example}

\newpage


\section{Nondeterministic finite automata }

Nondeterministic finite automaton can transit from a given state to none, one, or more than one state, consuming zero ($\varepsilon$), one, or more than one symbol.


\begin{definition}{Nondeterministic finite automaton}

A nondeterministic finite automaton (NFA) is a 5-tuple $(K, \Sigma,\Delta,s, F)$, where
\begin{enumerate}[label=]
  \item $K$ is a non-empty set of states
  \item $\Sigma$ is an alphabet
  \item $s \in K$ is the initial state
  \item $F \subseteq K$ is a set of final states
  \item $\Delta\subseteq K \times \Sigma^* \times K$ is a transition relation 
\end{enumerate}
\end{definition}

\newpage


\begin{definition}{Configuration of an NFA}

A configuration of $(K, \Sigma,\Delta, s, F)$ is any $(q, w) \in K \times \Sigma^*$.
\end{definition}  


\begin{definition}{Initial configuration of an NFA}

An initial configuration of $(K, \Sigma, \Delta,s, F)$ is any pair $(s, w)$, with $w \in \Sigma^*$.
\end{definition}  


\begin{definition}{Final configuration of an NFA}

A final configuration of $(K, \Sigma, \Delta,s, F)$ is any pair $(q, \varepsilon)$, with $q \in K$.
\end{definition}  


\begin{definition}{Direct transition of an NFA}

An NFA $(K, \Sigma, \Delta,s, F)$ performs a direct transition from configuration $(q, w)$ to configuration $(q', w')$, noted $(q, w) \vdash (q', w')$, iff
\begin{center}
  $\exists u \in \Sigma^* \suchthat (w=uw') \land ((q,u,q')\in\Delta)$
\end{center}
\begin{remark}
Transiting directly is not a function since for some configurations $(q,w)$ there can be more than one or none configuration $(q',w')$ such that $(q,w)\vdash(q',w')$.
\end{remark}
\end{definition}  

\begin{definition}{Blocked configuration}

	A blocked configuration of an NFA $M=(K,\Sigma,\Delta,s,F)$ is any non final configuration $(q,w)$ that verifies that $\nexists(q',w')\in K\times\Sigma^*\suchthat(q,w)\vdash(q',w')$. 

\end{definition}

\newpage


\begin{definition}{Transiting in $n$ steps}

  Let $(q,w)$ and $(q',w')$  be configurations of an NFA.
  
  \smallskip
  We say that $(q,w)$ transits in $n$ steps to $(q',w')$, noted $(q,w) \vdash ^n (q',w')$,
  with $n>1$, iff $\exists ~C_1, C_2, \ldots , C_{n-1}$ configurations of that NFA$\suchthat$
  \begin{center}
  	$(q,w) \vdash C_1,~C_1 \vdash C_2 ,~\ldots,~C_{n-1} \vdash (q',w')$
  \end{center}
  We say that $(q,w)$ transits in $1$ step to $(q',w')$, noted $(q,w) \vdash ^1 (q',w')$, iff
  \begin{center}
  	$(q,w)\vdash (q',w')$
  \end{center}
  
  We say that $(q,w)$ transits in $0$ step to $(q',w')$, noted $(q,w) \vdash ^0 (q',w')$, iff
  \begin{center}
  	$(q,w)= (q',w')$
  \end{center}
\end{definition}


\begin{definition}{Transiting in at least one step}

  Let $(q,w)$ and $(q',w')$ be configurations of an NFA.
  
  \smallskip
  We say that $(q,w)$ transits in at least one step to $(q',w')$,
  noted $(q,w) \vdash ^+ (q',w')$, iff
  \begin{center}
  	$\exists n>0\suchthat(q,w) \vdash ^n (q',w')$
  \end{center}
\end{definition}

\newpage


\begin{definition}{Transiting}

  Let $(q,w)$ and $(q',w')$ be configurations of an NFA.
  
  \smallskip
  We say that $(q,w)$ transits to $(q',w')$, noted $(q,w) \vdash ^* (q',w')$, iff
  \begin{center}
  	$\exists n\geq 0\suchthat(q,w) \vdash ^n (q',w')$
  \end{center}
  
\end{definition}

\begin{definition}{Computation. Length of a computation}

	Let $M$ be an NFA, a computation of $M$ is any sequence of configurations $C_0,C_1,C_2,...,C_n$ such as $C_i\vdash C_{i+1}$ $\forall i\in\{0,...,n-1\}$ with $n\geq0$.\\
	It will be noted as $C_0\vdash C_1\vdash C_2\vdash...\vdash C_n$, and the length of the computation is $n$.
\end{definition}

\begin{definition}{Well started/terminated/complete/blocked computation}

	A well started computation is any computation in which its first configuration is an initial configuration. \\
	A terminated computation is any computation in which its last configuration is a final configuration. \\
	A computation is complete if it is well started and terminated.\\
	A computation is blocked if its last configuration is blocked.
\end{definition}


\begin{definition}{Inaccessible state}

$ q \in K \text{ is an inaccessible state } (K, \Sigma, s, F, \Delta) \; \text{ iff } \; \nexists x \in \Sigma^* \suchthat (s, x) \vdash^* (q, \varepsilon) $
\end{definition}

\newpage


\begin{definition}{Accepted string}

	Let $M=(K,\Sigma,\Delta,s,F)$ an NFA and $w\in\Sigma^*$.
	$w$ is accepted by $M$ iff $\exists q\in F\suchthat(s,w)\vdash^*(q,\emptystring)$.
\end{definition}

\begin{definition}{Accepted language}

	Let $M$ an NFA, the language accepted by $M$ is defined as:
	\begin{center}
	$\Lb(M)=\{w\in\Sigma^* \suchthat w$ is accepted by $M\}$
	\end{center}
\end{definition}


\begin{definition}{Set of languages accepted by NFA}

The set of the languajes accepted by NFA is defined as:
\begin{center}
$\La(NFA) = \{L \suchthat \exists M \text{ NFA}, \Lb(M) = L\}$
\end{center}
\end{definition}


\begin{definition}{States diagram}

The state diagram of an NFA is a directed graph in which each node represents a state, and each edge represents a transition for a certain input between the states the edge conects (i.e. if $(q,w,q')\in\Delta$ then there will be an edge from $q$ to $q'$ labeled as $w$). Final states are represented with double circles and the initial state is indicated with "$>$".\\
\end{definition}

\newpage


\begin{example}

Let $M=(K,\Sigma,\Delta,s,F)$ be an NFA with:\\
$\begin{aligned}
K&=\{q_0,q_1\}\\
\Sigma&=\{0,1\}\\
\Delta&=\{(q_0,0,q_0),(q_0,1,q_0),(q_0,101,q_1),(q_0,1001,q_1),(q_1,0,q_1),(q_1,1,q_1)\}\\
s&=q_0\\
F&=\{q_1\}\\
\end{aligned}$

%% TBD: replace angle for 'start' label in initial nodes
%%      TikZ pgfmanual: http://www.texample.net/media/pgf/builds/pgfmanualCVS2012-11-04.pdf
%% \begin{tikzpicture}[every initial by arrow/.style={text=red,->>}]
%% \node[state,initial,initial distance=2cm] {$q_0$};
%% check:
%% \begin{tikzpicture}[every initial by diamond] {$q_0$};

\begin{center}
\begin{tikzpicture}[shorten >=1pt,node distance=8cm,on grid]
\node[state]   (q_0)                {$q_0$};
\node[inner sep=0pt,outer sep=-1pt,left=0pt of q0.west]{$>$};
\node[state,accepting]           (q_1) [right=of q_0] {$q_1$};
\path[->] (q_0) edge [loop above]   node         {1} ()
                edge [loop below]   node         {0} ()
                edge [bend left] node [above] {101} (q_1)
                edge [bend right]   node [below] {1001} (q_1)
          (q_1)	edge [loop above]   node         {0} ()
                edge [loop below]   node         {1} ();
\end{tikzpicture}
\end{center}
\end{example}

\newpage


\section{Minimum deterministic finite automaton}

\begin{definition}{Equivalent finite automata ($M_1\equiv M_2$)}

Two finite automata, $M_1$ and $M_2$ are equivalent iff:
\begin{center}
$L(M_1)=L(M_2)$
\end{center}
\end{definition}


\begin{definition}{Minimum deterministic finite automaton (MDFA)}

For a DFA $M=(K,\Sigma,\delta,s,F)$, $M$ is a MDFA iff it verifies:
\begin{center}
$M'\equiv M \Rightarrow \card{K'} \geq \card{K}, \; \forall M'=(K',\Sigma,\delta',s',F') $
\end{center}
\end{definition}

\begin{proposition}

$ \forall M$ DFA $\exists M' \text{ MDFA } \suchthat M \equiv M'$
\end{proposition}


\section{Equivalences}

\begin{definition}{Set of the languages represented by finite automata}
\begin{center}
$\La(\FA) = \La(DFA) = \La(NFA)$
\end{center}
\end{definition}


\begin{theorem}

$L\in \La(\RE)\Leftrightarrow L\in \La(\FA)$
\end{theorem}


\begin{theorem}

$L\in \La.3\Leftrightarrow L\in \La(\FA)$
\end{theorem}


\begin{corollary}

$\La.3 = \La(\RE) = \La(\FA)$
\end{corollary}

