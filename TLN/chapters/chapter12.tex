%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHAPTER 12: Theoretical limits of computing
\chapter{Theoretical limits of computing}
\localtableofcontents

\newpage


\section{Halting problem}

\begin{definition}{Problem associated to a predicate}

\begin{center}
\begin{tabular}{ |l l|l|l| }
  \hline
  \textbf{Problem}    &                                      & \textbf{Predicate} & \textbf{Function} \\ 
  \hline
   solvable           & \translation{resoluble}              & decidable          & $\in TREC$        \\
  \hline
   partially solvable & \translation{parcialmente resoluble} & enumerable         & $\in REC$         \\
  \hline
   unsolvable         & \translation{irresoluble}            & undecidable        & $\notin TREC$     \\
  \hline
   totally unsolvable & \translation{totalmente irresoluble} & not enumerable     & $\notin REC$      \\
  \hline
\end{tabular}
\end{center}
\end{definition}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% halting problem
\begin{definition}{$H$ predicate}
 
Given $Q \in \WHILE$, $g = while2N(Q)$, and $\underline{x} \in \mathbb{N}^*$,
\begin{center}
$H(g,\vec{x}) \Leftrightarrow F_Q(\vec{x}) \in \mathbb{N}$

\remark $H(g,\vec{x})$ is true iff $Q$ halts with input $\vec{x}$.
\end{center}
 
\end{definition}


\begin{definition}{$H^n$ predicate}
 
Given $Q \in \WHILE \suchthat F_Q \in \FWHILEN{n}$, $g = while2N(Q)$, and $\underline{x} \in \mathbb{N}^n$.
\begin{center}
$H^n(g,\vec{x}) \Leftrightarrow F_Q(\vec{x}) \in \mathbb{N}$.
\end{center}

\remark From now on, $g$ will be referred as 'a program' (i.e. the program it encodes), or 'the number' of a program.

\end{definition}
 
\newpage


\begin{theorem}{Problem $H^1$ is not solvable}
 
\begin{proof} By contradiction:

\begin{center}
$H^1 \text{ solvable } \; \Rightarrow \; H^1 \text{ decidable} \; \Rightarrow \; \exists h^1 \in \TREC \suchthat h^1 = X_{V_{H^1}} $
\end{center}

\begin{center}
$h^1(g,x) =
\begin{cases}
0 & \text{if } \overline{H^1(g,x)} \\
1 & \text{if } H^1(g,x)
\end{cases}$
\end{center}

Let's consider the function
\begin{center}
$bomb(x) =
\begin{cases}
1        & \text{if }h^1(x,x)=0 \\
\uparrow & \text{if }h^1(x,x)=1
\end{cases}$
\end{center}

\whileprogram*{Bomb}{1}{s}{
\whilesentence{\text{\codesetfont H1(\X{1},\X{1})}}{
  \copysentence*{\X{1}}{\X{1}}
}
\incrsentence{\X{1}}{\X{2}}
}

If $b = while2N(Bomb)$ then

$\begin{aligned}
\tab\tab\tab\tab h^1(b,b) = 1 \; &\Rightarrow \; bomb(b) = \;\uparrow \; &\Rightarrow \; h^1(b,b)=0 \;\;\; \Contradiction \\
\tab\tab\tab\tab h^1(b,b) = 0 \; &\Rightarrow \; bomb(b) = 1          \; &\Rightarrow \; h^1(b,b)=1 \;\;\; \Contradiction
\end{aligned}$

\end{proof}
\end{theorem}


\begin{corollary}{$H$ is not solvable}
 
\remark
$H^1 \text{ not solvable } \Rightarrow H^n \text{ not solvable } \Rightarrow H \text{ not solvable}$.
\end{corollary}


\begin{proposition}{$H^1$ is partially solvable}

\begin{proof} Let's consider

$f : \mathbb{N}^2 \rightarrow \mathbb{N}$\\
$f = \sigma \left(U [\REC^1] \right)$\\
\begin{center}
$P_f = H^1 \subcomment{\;\Rightarrow\;}{f \in \REC} H^1 \in \PREDREC \;\;\Rightarrow \;\; H^1 \text{ is partially solvable}$.
\end{center}
\end{proof}

\remark Being $H^1$ enumerable means that all the pairs $(g, x) \in \mathbb{N} \suchthat F_{N2while(g)}(x) \in \mathbb{N}$ can be generated.
This can be done by simulating each program with a given input for a number of steps, and checking if it has halted.
\end{proposition}


\begin{corollary}{$H$ is partially solvable}
 
\remark
$H^1 \text{ partially solvable } \Rightarrow H^n \text{ partially solvable } \Rightarrow H \text{ partially solvable}$.
\end{corollary}

\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% busy beaver
\section{The busy beaver function}

\begin{definition}{Length of a $\WHILE$ program \translation{longitud de un programa \WHILE}}

$length: \WHILE \rightarrow \mathbb{N}$\\
$length\bigr((n,s)\bigl) = \card{s}_{do} + \card{s}_{\Assig}$

\end{definition}


\begin{definition}{Set of the While programs of lenght $n \left(\WHILEN{n}\right)$}

$\WHILEN{n} = \{Q \in \WHILE \suchthat length(Q) = n\}$

\end{definition}

 
\begin{definition}{Busy beaver function ($\Sigma$})

$\Sigma : \mathbb{N} \to \mathbb{N}$

$\Sigma (n) = max \{F_Q(0) \suchthat F_Q \in \FWHILEN{1} \land Q \in \WHILEN{n} \land F_Q(0) \in \mathbb{N}\}$
\end{definition}

\newpage


%% first property of busy beaver %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}{$\Sigma(n+1) > \Sigma(n)$}

\begin{proof} Let's consider $Q = (1,s) \in \WHILEN{n} \ \land \ F_Q(0)=\Sigma(n)$, then

\whileprogram*{Q'}{1}{s'}{
$s$ ;\\
\incrsentence*{\X{1}}{\X{1}}
}

$\begin{aligned}
length(Q') = n+1 \land F_{Q'}(0) = \Sigma(n)+1 \ &\Rightarrow\\
\Sigma(n+1) \geq \Sigma(n)+1  > \Sigma(n) \ \ &\Rightarrow \ \Sigma(n+1) > \Sigma(n)
\end{aligned}$

\end{proof}

\end{proposition}


%% second property of busy beaver %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{proposition}{$\exists Q \in \WHILEN{k} \suchthat f=F_Q \Rightarrow \Sigma(n+k) \geq f(n)$}

\begin{proof} Let's consider $Q=(1,s) \in \WHILEN{k} \land F_Q=f$, then

\whileprogram*{Q'}{1}{s'}
{
\incrsentence{\X{1}}{\X{1}}
% TBD: reformat this brace
$\quad\quad\cdots$
\hspace*{3.5em}\rlap{\smash{$\left.
\begin{array}
{@{}c@{}}\\{}\\\vspace{0.3em}{}
\end{array}
\right\}
\begin{tabular}{l}$n$ times
\end{tabular}$}}\\
% -------------------------
\incrsentence{\X{1}}{\X{1}}
$s$
}

$Q' \in \WHILEN{n+k} \land F_{Q'}(0) = f(n) \ \Rightarrow \ \Sigma(n+k) \geq f(n)$
\end{proof}

\end{proposition}

\newpage


\begin{theorem}{$\Sigma \notin \FWHILE$}
 
\begin{proof} By contradiction: let's consider
 
\begin{equation*}
\left.
\begin{aligned}
\Sigma \in \text{$\FWHILE$}\\
\text{let } g(n)=2n \Rightarrow g \in \text{$\FWHILE$}\\
\text{let } f(n) = \Sigma (2n)
\end{aligned}
\right\}
\quad \Rightarrow \hspace{2mm} f \in \text{$\FWHILE$} \hspace{2mm} \Rightarrow
\end{equation*}

$\exists Q \in \WHILE^k \suchthat F_Q = f \subcomment{\Rightarrow}{\text{prop. 12.2.2}} \Sigma(n+k) \geq f(n), \ \forall n \in N \subcomment{\Rightarrow}{f(n)=\Sigma(2n)}$

$\Sigma(n+k) \geq \Sigma(2n), \ \forall n \in \N \subcomment{\Rightarrow}{n=k+1} \Sigma (2k+1) \geq \Sigma (2k+2) \subcomment{\Contradiction}{\text{prop. 12.2.1}}$

\end{proof}

\end{theorem}


\begin{proposition}{$\Sigma \not \in \FWHILE \hspace{4mm} \Rightarrow \hspace{2mm} H^1$ is not solvable}
 
\begin{proof} By contradiction: let's consider $\Sigma \notin \FWHILE \ \land \ H^1$ is solvable.
 
Since there are a finite number of (equivalent) programs of a given length, then the program that halts (with zero input) for each length can be found, and so the biggest output.

This implies $\Sigma \in \FWHILE \hspace{5mm} \Contradiction$

\end{proof}
\end{proposition}
