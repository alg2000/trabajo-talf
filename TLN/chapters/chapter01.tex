\usetikzlibrary{positioning,chains,fit,shapes,calc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHAPTER 1: PREAMBLE TO DISCRETE MATHEMATICS
\chapter{Preamble to discrete mathematics}

\localtableofcontents
\initialquote{\\No one shall expel us from the paradise that Cantor has created.}{David Hilbert}

\newpage


%%%%% FIRST SECTION
\section{Reminder}


\begin{definition}{Power set \translation{conjunto potencia}}
  \begin{center}
    $ \mathcal{P}(A) = \{ B \suchthat B \subseteq A \} $
  \end{center}
  \begin{remark}
    $\emptyset, A \in \mathcal{P}(A)$
  \end{remark}
\end{definition}
 

\begin{definition}{Proper subset \translation{subconjunto propio}}

  $B$ is a proper subset of $A$ iff
  \begin{center}
    $ B \subset A \land B \neq \emptyset $
  \end{center}
\end{definition}


\begin{center}

\def \ellipsezero {(0.0, 0.0) ellipse  (12.0cm and 8.0cm)}
\def \ellipseone {(4.0, 0.0) ellipse  (6.0cm and 4.0cm)}

\begin{tikzpicture}[scale=0.5]
  \draw \ellipsezero;
  \draw \ellipseone;
  \node[label={$A$}]  (c1) at (-6.5, 2.0){$\{1,2,3,4,5\}$};
  \node[label={$B$}]  (c2) at (4.0, -1.0){$\{3,4,5\}$};
  
\end{tikzpicture}
\end{center}

\begin{definition}{Partition \translation{partición}}

  $\Pi=\{A_1,A_2,\dots \} \subseteq \mathcal{P}(A)$ is a partition of $A$ iff
  \begin{itemize}
    \item $ \bigcup\limits_{A_i \in \Pi}^{} A_i = A $
    \item $ A_i \cap A_j = \emptyset, \forall A_i \neq A_j $
    \item $ A_i \neq \emptyset, \forall A_i \in \Pi $
  \end{itemize}
\end{definition}




\begin{definition}{Cartesian product \translation{producto cartesiano}}
  \begin{center}
    $A \times B = \{(a,b)\suchthat a \in A, b \in B \}$
  \end{center}
  \begin{remark}
    $(a, b)$ is an ordered pair \translation{par ordenado}.
  \end{remark}
\end{definition}


\begin{definition}{Relation \translation{relación}}

  $R \subseteq A \times B$ is a relation from $A$ to $B$.
\end{definition}


\begin{definition}{Binary relation \translation{relación binaria}}

  $R \subseteq A \times A$ is a binary relation on $A$.
\end{definition}
\newpage

\begin{definition}{Properties of binary relations}

	\begin{itemize}
	 \item $R$ is Reflexive iff     $(a,a) \in R ~~\forall a \in A$
	 \item $R$ is Symmetric iff     $(a,b) \in R \Rightarrow (b,a) \in R$
	 \item $R$ is Antisymmetric iff $(a,b) \in R \wedge a \neq b \Rightarrow (b,a) \notin R$ 
   \item $R$ is Transitive iff    $(a,b) \in R \wedge (b,c) \in R \Rightarrow (a,c) \in R$
	\end{itemize}
\end{definition}



\begin {definition}{Equivalence relation \translation{relación de equivalencia}}
\\
$R$ is an equivalence relation iff it is reflexive, symmetric and transitive.
\end {definition}

\begin {definition}{Equivalence class \translation{clase de equivalencia}}

Being $A$ a set, $R$ an equivalence relation on $A$ and $a \in A$, $[a]$ is an equivalence class defined as 
\begin{center} $[a] = \{b \in A \suchthat (a,b) \in R\}$
\end{center}
\begin{remark}
An equivalence relation of a set defines a partition for that set (quotient set) where each element of the partition is an equivalence class.
\end{remark}
\end {definition}

\newpage


\begin {definition}{Identity relation $I$ \translation{relación identidad}}
\begin{center}
$I = \{(a,a) \suchthat a \in A\}$.
\end{center}
\end {definition}

\begin {definition}{Inverse relation $R^{-1}$ \translation{relación inversa}}
\begin{center}
$R^{-1} = \{(a,b) \suchthat (b,a) \in R\}$
\end{center}
\end {definition}

\begin {definition}{Power of a relation $R^n$ \translation{potencia de una relación}}

Given $ R \subseteq A \times A $,

\begin{center}
$ R^n = \begin{cases}
R & n=1 \\
\left\{(a,b) \suchthat \exists x \in A, (a,x)\in R^{n-1}\wedge (x,b)\in R\right\} & n > 1
\end{cases}$
\end{center}
\end {definition}

\begin {definition}{Closure of relations \translation{cierre de relaciones}}

The closure of a relation under a certain property is the smallest relation which contains the first one and satisfies that property.
\begin {itemize}
\item Reflexive closure of $R$ is $R \cup I$
\item Symmetric closure of $R$ is $R \cup R^{-1}$
\item Transitive closure of $R$ is $ R^{\infty} = \bigcup\limits_{n=1}^{\infty}R^{n} $
\end{itemize}
\end {definition}

\begin{example}
Given $A = \{a, b, c\}$, and the binary relation $R = \{(b, c), (c, a), (a, c)\}$

\qquad $R^2 = \{(b, a), (c, c), (a, a)\}$

\qquad $R^3 = \{(b, c), (c, a), (a, c)\} = R$

\qquad $R^\infty = \{(b, c), (c, a), (a, c), (b, a), (c, c), (a, a)\}$

\end{example}



\newpage


\begin{definition}{Function \translation{función}}
  \begin{center}
    $f: A \rightarrow B$ is a function iff $\forall a \in A \; \exists !\; (a,b) \in f$.
  \end{center}
  \begin{remark}
   "$!$" denotes "one and only one".
  \end{remark}
\end{definition}


\begin {definition}{Domain and range of a function \translation{dominio y rango}}

Given $f: A \rightarrow B$,

\qquad $Dom(f) = \{a \in A \suchthat f(a) = b, b\in B\}$

\qquad $Rg(f) = \{f(a) \in B \suchthat a \in A\}$

\end {definition}


\begin{definition}{Injective function \translation{función inyectiva}}

$f:A \rightarrowtail B$ iff $f(x) = f(z) \Rightarrow x = z$
\remark{This also means that $x\neq z \Rightarrow f(x)\neq f(z)$}
\end {definition}

\begin{definition}{Surjective function \translation{función sobreyectiva}}

$f:A \twoheadrightarrow B$ iff $Rg(f) = B$
\end{definition}

\begin {definition}{Bijective function \translation{función biyectiva}}

$f:A \leftrightarrow B$ iff it is injective and surjetive.
\end {definition}

\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure
\begin{example}
~\par\addvspace{1ex}
\noindent
% injective function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{minipage}{0.32\textwidth}
\begin{tikzpicture}[thick,
  every fit/.style={
  text width=2cm},->,
]
% vertices of A
\begin{scope}[start chain=going below,node distance=7mm]
\foreach \i in {1,2,...,4}
  \node[on chain] (f\i) [label=left: \i] {};
\end{scope}
% vertices of B
\begin{scope}[xshift=4cm
	,start chain=going below,node distance=7mm]
\foreach \i in {a,b,c,d,e}
  \node[on chain] (s\i) [label=right: \i] {};
\end{scope}
% set A
\node [fit=(f1)(f4),label=above:$A$] {};
% set B
\node [fit=(sa)(se),label=above:$B$] {};
% edges
\draw (f1) -- (sa);
\draw (f2) -- (sb);
\draw (f3) -- (sc);
\draw (f4) -- (sd);;
\end{tikzpicture}
\end{minipage} \hfill
% surjective function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{minipage}{0.32\textwidth}
\begin{tikzpicture}[thick,
  %every %node/.style={draw,circle},
  %fsnode/.style={fill=white},
  %ssnode/.style={fill=white},
  every fit/.style={%ellipse,draw,
  text width=2cm},->,%shorten >= 3pt,shorten <= 3pt
]
% vertices of A
\begin{scope}[start chain=going below,node distance=7mm]
\foreach \i in {1,2,...,5}
  \node[on chain] (f\i) [label=left: \i] {};
\end{scope}
% vertices of B
\begin{scope}[xshift=4cm,start chain=going below,node distance=7mm]
\foreach \i in {a,b,c,d}
  \node[%ssnode,
  				on chain] (s\i) [label=right: \i] {};
\end{scope}
% set A
\node [fit=(f1) (f4),label=above:$A$] {};
% set B
\node [fit=(sa) (se),label=above:$B$] {};
% edges
\draw (f1) -- (sa);
\draw (f2) -- (sb);
\draw (f3) -- (sc);
\draw (f4) -- (sd);
\draw (f5) -- (sd);
\end{tikzpicture}
\end{minipage} \hfill
% bijective function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{minipage}{0.32\textwidth}
\begin{tikzpicture}[thick,every fit/.style={text width=2cm},->
]
% vertices of A
\begin{scope}[start chain=going below,node distance=7mm]
\foreach \i in {1,2,...,5}
  \node[%fsnode,
  			on chain] (f\i) [label=left: \i] {};
\end{scope}
% vertices of B
\begin{scope}[xshift=4cm,start chain=going below,node distance=7mm]
\foreach \i in {a,b,c,d,e}
  \node[on chain] (s\i) [label=right: \i] {};
\end{scope}
% set A
\node [fit=(f1) (f4),label=above:$A$] {};
% set B
\node [fit=(sa) (se),label=above:$B$] {};
% edges
\draw (f1) -- (sa);
\draw (f2) -- (sb);
\draw (f3) -- (sc);
\draw (f4) -- (sd);
\draw (f5) -- (se);
\end{tikzpicture}
\end{minipage} \hfill
\captionof{figure}{Three examples of functions: injective function (left), surjective function(center), and bijective function (right).} \label{fig:functions}
\end{example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage


\newpage

\begin {definition}{Closure of a set under an operation}

A set is closed under an operation iff this operation can be defined as a function \begin{center} $f:A \times A \rightarrow A$ \end{center}
\end {definition}


\begin{definition}{Associative property \translation{propiedad asociativa}}

Being $A$ a closed set for the operation $\bullet$,  ($\bullet:A \times A \rightarrow A$), $\bullet$ is associative iff \begin{center} $(x\bullet y)\bullet z = x\bullet (y\bullet z) \: \forall x,y,z \in A$ \end{center}
\end{definition}


\begin{definition}{Semigroup \translation{semigrupo}}

A semigroup is a pair $(A,\bullet)$ where $A$ is a set closed under $\bullet$, which is an associative operation. 
\end{definition}


\begin{definition}{Neutral element \translation{elemento neutro}}

Also called identity element, $e \in A$ is the neutral element for an operation $\bullet$ iff \begin{center} $\forall a \in A$ $\;a\bullet e = e\bullet a = a$ \end{center}
\end{definition}


\begin{definition}{Monoid \translation{monoide}}

It is a semigroup with a neutral element for the operation.
\end{definition}

\newpage

\begin{definition}{Strict closure of a set under an operation \translation{cierre estricto}}

Being $\bullet$ an operation on $A$ and $B \subseteq A$, $B^{\bullet}$ is the strict closure of $B$ for $\bullet$ defined as:
\begin{itemize}
\item $x \in B \Rightarrow x \in B^{\bullet}$
\item $x,y \in B^{\bullet} \Rightarrow x\bullet y \in B^{\bullet}$
\item no other element belongs to $B^{\bullet}$.
\end{itemize}
\end{definition}


\begin{definition}{Wide closure of a set under an operation \translation{cierre amplio}}

Being $(A,\bullet)$ a monoid and $B \subseteq A$, the wide closure of $B$ under $\bullet$ is 
\begin{center}
$B^{\bullet e} = B^{\bullet} \cup \{e\}$
\end{center}
\end{definition}

\newpage


%%%%% SECOND SECTION
\section{Finite and Infinite Sets}


\begin{definition}{Equipotent sets \translation{conjuntos equipotenciales}}

$A \sim B$ (are equipotent) iff $\exists f: A \leftrightarrow B$ 
\end{definition}


\begin{definition}{Cardinality of a set \translation{cardinalidad de un conjunto}}
\begin{center}
$\card{A} = 
\begin{cases}
0                & \text{if } A=\emptyset \\
n \in \mathbb{N} & \text{if } A \sim \{1,2,...,n\}\\
\aleph_0         & \text{if } A \sim \mathbb{N} \\
\aleph_i         & \text{if } A \sim \mathcal{P}(\aleph_{i-1}), i > 0
\end{cases}$
\end{center}
\begin{remark}
\end{remark}
\begin{itemize}[label={}]
  \item $\emptyset = \{\,\}$ is the empty set \translation{conjunto vacío}
  \item $\card{\mathbb{N}} = \aleph_0$ (\textit{aleph-null} \translation{álef cero})
  \item $\card{\mathbb{R}} = \aleph_1$ (\textit{aleph-one} \translation{álef uno}), according to the continuum hipothesis.
\end{itemize}
\end{definition}

\begin{proposition}
$\aleph_1 - \aleph_0 = \aleph_1$
\end{proposition}


\begin{definition}{Finite set \translation{conjunto finito}}

$A$ is a finite set iff $\card{A} \in \mathbb{N}_0$.
\end {definition}


\begin{definition}{Infinite set \translation{conjunto infinito}}

$A$ is an infinite set iff $\card{A}$ is not finite.
\begin{remark}
$A$ is infinite iff $\card{A} \in \{\aleph_i : i \in \mathbb{N}_0 \}$.
\end{remark}
\end{definition}


\begin{definition}{Countable infinite set \translation{conjunto infinito numerable}}

$A$ is a countable infinite set iff $A \sim \mathbb{N}$.
\end{definition}


\begin{definition}{Countable set \translation{conjunto numerable}}

$A$ is countable if it is finite or countable infinite.
\end{definition}


\begin{definition}{Uncountable set \translation{conjunto no numerable}}

$A$ is uncountable if it is not countable.
\begin{remark}
$\mathbb{R}$ is an uncountable set.
\end{remark}
\end{definition}


\begin{proposition}
Any subset of a countable set is countable.
\end{proposition}


\begin{proposition}
The finite union of countable sets is countable.
\end{proposition}


\begin{proposition}
Countable infinite union of countable sets is countable.
\end{proposition}


\begin{proposition}
The cartesian product of two countable sets is countable.
\end{proposition}


\begin{proposition}
The finite power of a countable set is countable.
\end{proposition}

\newpage


\begin{theorem}{Canthor's theorem \translation{teorema de Cantor}}
\begin{center}
$\card{A} < \card{\mathcal{P}(A)}$
\end{center}
\begin{remark}
$\card{\mathbb{N}} < \card{\mathcal{P}(\mathbb{N})} \Rightarrow \aleph_0 < \aleph_1 \Rightarrow \card{\mathbb{N}} < \card{\mathbb{R}}$
\end{remark}
\end{theorem}

\begin{figure}[h]
\begin{center}
\includegraphics[width=8cm]{GeorgCantor}
\end{center}
\caption{\href{https://en.wikipedia.org/wiki/Georg_Cantor}{Georg Cantor} defined the transfinite numbers by the end of the XIX century.}
\end{figure}




