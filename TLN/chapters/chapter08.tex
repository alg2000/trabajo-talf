%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHAPTER 8: Recursive functions
\chapter{Recursive functions}
\localtableofcontents
\newpage
\section{Formal definition}

\begin{definition}{Zero function $(\theta)$ \translation{función cero}}

$\theta: \mathbb{N}^0 \to \mathbb{N}$

$\theta()=0$
\end{definition}

\begin{definition}{Successor function \translation{función sucesor}}

$\sigma: \mathbb{N} \to \mathbb{N}$

$\sigma(n)=n+1$
\end{definition}

\begin{definition}{Projection function $\pi _i^k$ \translation{función proyección de $k$ argumentos}}

For each pair $(k,i) \in \mathbb{N}^2$ with  $k \geq 1$ and $1\leq i\leq k$ the $k$-ary projection function that returns the $i$-th argument is defined as: \newline
$\pi _i^k:\mathbb{N}^k \to \mathbb{N}$
$\pi _i^k(n_1,\dots,n_k)=n_i $

The set of of all the projection functions is represented as $\pi$, that is:

\begin{center}
$\pi=\{\pi _i^k \suchthat (k,i)\in \mathbb{N}^2$ with $k\geq 1 \wedge 1\leq i\leq k\}$
\end{center}
\end{definition}


\begin{definition}{Initial functions \translation{funciones iniciales}}

\begin{center}
$\INI=\pi \cup \{\theta , \sigma \}$
\end{center}
\end{definition}

\newpage

\begin{definition}{Composition \translation{comopsición de funciones}}

Let $m>0, k\geq0 $ and the functions: 

$g:\mathbb{N}^m \to \mathbb{N}$

$h_1, \dots, h_m: \mathbb{N}^k \to \mathbb{N}$

If the function $f:\mathbb{N}^k\to \mathbb{N}$ is:

\begin{center}
    $f(\vec{n})=g(h_1(\vec{n}), \dots, h_m(\vec{n})) $
\end{center}
then $f$ is obtained by composition of $g$ and $h_1$, \dots, $h_m$, 
which is expressed as $f(\vec{n})=g(h_1, \dots, h_m)(\vec{n})$, or simply $f=g(h_1, \dots, h_m)$.
\end{definition}


\begin{definition}{Primitive recursion \translation{recursión primitiva}}
\newline Let $k\geq 0$ and the functions
\newline $g:\mathbb{N}^k \to \mathbb{N}$
\newline $h:\mathbb{N}^{k+2} \to \mathbb{N}$
\newline If the function $f:\mathbb{N}^{k+1} \to \mathbb{N}$ is:

\begin{center}
    $f(\vec{n},m)=\left\{ 
\begin{array}{lcc}
    g(\vec{n})                          & \text{if} & m=0\\
    h(\vec{n},m-1,f(\vec{n},m-1)) & \text{if} & m>0 
\end{array}\right.$
\end{center}

then $f$ is obtained from $g$ and $h$ by primitive recursion.

We will express it as $f(\vec{n})=\rec{g}{h}(\vec{n})$, or simply $f=\rec{g}{h}$.
\end{definition}


\begin{definition}{Minimization \translation{minimización no acotada}}
\newline Let $k\geq 0$ and the function:
\newline $g:\mathbb{N}^{k+1} \to \mathbb{N}$
\newline If the function $f:\mathbb{N}^k \to \mathbb{N}$ is:
\begin{center}
    $f(\vec{n})=\left\{ 
\begin{array}{ll}
minimum(A) & \qquad \text{if} \quad A \neq \emptyset \wedge \forall t \leq minimum(A)\;\;g(\vec{n},t) \in \mathbb{N}  \\
\uparrow   & \qquad \text {otherwise}
\end{array}\right.$
\end{center}
where $A=\{t\in \mathbb{N} \suchthat g(\vec{n},t)=0\}$ and $\vec{n}\in \mathbb{N}^k$.
Then, $f$ is obtained from $g$ by minimization.

We will express it as $f(\vec{n})=\mnz{g}(\vec{n})$, or simply $f=\mnz{g}$.
	\begin{remark}
	The symbol $"\uparrow"$ means that the function is not defined (diverges) for that input.
	\end{remark}
\end{definition}

\newpage


%% examples of recursive functions
\begin{example}{$add$ is a recursive function}

\begin{center}
$\begin{aligned}
&add:\mathbb{N}^2 \to \mathbb{N}\\
&add(x,y) = x + y
\end{aligned}$
\end{center}


It can be defined like:

\begin{center}
$add = \langle \pi^1_1 | successor_3 \rangle $
\end{center}

where 
\begin{center}
$\begin{aligned}
&successor_3 : \mathbb{N}^3 \to \mathbb{N}\\
&successor_3(x,y,z)=z +1
\end{aligned}$
\end{center}

is also a recursive function, because it can be defined as
\begin{center}
$successor_3 = \sigma (\pi^3_3)$
\end{center}

yielding
\begin{center}
$add = \rec{\pi^1_1}{\sigma(\pi^3_3)} $
\end{center}

Similarly, the $subtract$ function is also a recursive function
\begin{center}
$subtract = \rec{\pi^1_1}{\rec{\theta}{\pi^2_1}(\pi^3_3)} $
\end{center}
\end{example}

\newpage


\begin{example}{Adding two numbers recursively}

$\\
\begin{aligned}
add(4,3) &= \rec{\pi^1_1}{\sigma(\pi^3_3)}(4,3) \\
         &= \sigma (\pi^3_3)(4,2,\rec{\pi^1_1}{\sigma(\pi^3_3)}(4,2)) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,\rec{\pi^1_1}{\sigma(\pi^3_3)}(4,1))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,\sigma (\pi^3_3)(4,0, \rec{\pi^1_1}{\sigma(\pi^3_3)}(4,0)))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,\sigma (\pi^3_3)(4,0,\pi _1^1(4)))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,\sigma (\pi^3_3)(4,0,4))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,\sigma (\pi^3_3(4,0,4)))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,\sigma (4))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3)(4,1,5)) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(\pi^3_3(4,1,5))) \\
         &= \sigma (\pi^3_3)(4,2,\sigma(5)) \\
         &= \sigma (\pi^3_3)(4,2,6) \\
         &= \sigma (\pi^3_3(4,2,6)) \\
         &= \sigma (6) \\
         &= 7
\end{aligned} $



\end{example}

\newpage


\begin{example}{$product$ is a recursive function}

\begin{center}
$\begin{aligned}
&product:\mathbb{N}^2 \to \mathbb{N}\\
&product(x, y) = x \cdot y
\end{aligned}$
\end{center}

and it is a recursive function, because it can be defined like:

\begin{center}
$product = \rec{\theta^1}{add^3_{1,3}} $
\end{center}

being $\theta^1$ and $add^3_{1,3}$ recursive functions as well:
\begin{center}
\begin{tabular}{ l @{\hskip 5cm} l }
$\theta^1 : \mathbb{N}^1 \to \mathbb{N}$        & $add^3_{1,3} : \mathbb{N}^3 \to \mathbb{N}$ \\
$\theta^1(x)=0$                                 & $add^3_{1,3}(x, y, z) : x + z$ \\
$\theta^1(x)= \rec{\theta}{\pi^2_2}$ & $add^3_{1,3} = add(\pi^3_1, \pi^3_3) = \rec{\pi^1_1}{\sigma (\pi _3^3)} (\pi^3_1, \pi^3_3)$\\
\end{tabular}\\
\end{center}

yielding
\begin{center}
$product = \rec{\rec{\theta}{\pi^2_2}} {\rec{\pi^1_1}{\sigma (\pi^3_3)} (\pi^3_1, \pi^3_3)} $\\
\end{center}
\end{example}

\newpage


\begin{example}{$divide$ is a recursive function}

\begin{center}
$\begin{aligned}
&divide:\mathbb{N}^2 \to \mathbb{N}\\
&divide(x, y) = \frac{x}{y}
\end{aligned}$
\end{center}

it can be defined with the minimization operator:

\begin{center}
$divide = \mnz{subtract(\pi^3_1, product(\pi^3_2, \pi^3_3))} $
\end{center}
namely,
\begin{center}
$divide = \mnz{\rec{\pi^1_1}{\rec{\sigma}{\pi^2_2}(\pi^3_3)}(\pi^3_1, \rec{\rec{\sigma}{\pi^2_2}} {\rec{\pi^1_1}{\sigma (\pi _3^3)} (\pi^3_1, \pi^3_3)}(\pi^3_2, \pi^3_3))} $
\end{center}
In this approach, the quotient is found in a search process, as the minimum number which multiplied by the divisor equals (or exceeds) the dividend.\\
For example,

\begin{center}
$\begin{aligned}
&divide(6, &3) = 2 &\text{, since } 6 - 3 \cdot 0 = 6, 6 - 3 \cdot 1 = 3 \text{ and } 6 - 3 \cdot 2 = 0\\
&divide(25,&3) = 9 &\text{, since } 25 - 3 \cdot 8 = 1 \text{ and } 25 - 3 \cdot 9 = 0
\end{aligned}$
\end{center}

\end{example}

\newpage
%% end of examples of recursive functions

\begin{definition}{Total function, Partial functions}

\begin{center}
$\begin{aligned}
&f:\mathbb{N}^k \to \mathbb{N}, k>0 \text{ is a total function if } Dom(f)=\mathbb{N}^k\\
&f:\mathbb{N}^0 \to \mathbb{N} \text{ is a total function if } f()\in \mathbb{N}
\end{aligned}$
\end{center}

A function is a partial functon iff it is not total.
\end{definition}


\begin{definition}{Set of the recursive functions}

The set $\REC$ is defined as
\begin{enumerate}[label=\arabic*.]
\item $\INI\subset \REC$
\item $g,h_1, \dots, h_m \in \REC \wedge \exists g(h_1, \dots, h_m) \Rightarrow g(h_1, \dots, h_m)\in \REC$ 
\item $g, h \in  \REC \wedge \exists \langle g|h \rangle \Rightarrow \langle g|h \rangle \in \REC$
\item $g \in \REC \wedge \exists \mnz{g} \Rightarrow \mnz{g} \in  \REC$
\item No other function is in $\REC$.
\end{enumerate}

$f$ is a recursive function iff $f \in \REC$.

\end{definition}


\begin{definition}{Total recursive functions}

\begin{center}
$\TREC = \{f\in \REC \suchthat f \text{ is total}\}$
\end{center}

\end{definition}

\newpage


\section{Enumerability, decidability, computability}

\begin{definition}{Set of truth values for a predicate \translation{valores de verdad}}

The set of truth values for a predicate $P$, of $k\geq 0$ arguments is
\begin{center}
$V_P=\{\vec{x}\in \mathbb{N}^k \suchthat P(\vec{x})\}$ 
\end{center}

\end{definition}

\begin{definition}{Predicate associated to a function}

The predicate associated to the function $f$ of $k\geq 0$ arguments, is
\begin{center}
$P_f(\vec{x})=\left\{
\begin{array}{ll}
true & \quad \text{if} \quad f(\vec{x})>0  \\
false & \quad otherwise
\end{array}\right.$
\end{center}


\end{definition}

\begin{definition}{Characteristic function of a set \translation{función característica}}

The characteristic function of $V \subseteq \mathbb{N}^k$, $X_V \suchthat \mathbb{N}^k\to \mathbb{N}$, with $k\geq 0$, is
\begin{center}
$X_V(\vec{x})=\left\{
\begin{array}{lrc}
1 & \quad \text{if} & \vec{x}\in V  \\
0 & \quad \text{if} & \vec{x}\not\in V
\end{array}\right.$
\end{center}


\end{definition}

\begin{definition}{Characteristic function of a predicate}

$X_{V_P}$ is the characteristic function of predicate $P$.
\end{definition}

\newpage


\begin{definition}{Recursively decidable predicates \translation{predicado r.d.}}

\begin{center}
$\PREDTREC = \{P_f \suchthat f\in \TREC\}$ 
\end{center}

\end{definition}

\begin{definition}{Recursively enumerable predicates \translation{predicado r.e.}}

\begin{center}
$\PREDREC = \{P_f \suchthat f \in \REC\}$
\end{center}
\begin{remark}
$\PREDTREC\subset\PREDREC$ since $\TREC\subset\REC$.
\end{remark}
\end{definition}

\begin{definition}{Recursively decidable sets}

\begin{center}
$\DEC = \{V_P \suchthat P\in \PREDTREC\}$
\end{center}
\end{definition}

\begin{definition}{Recursively enumareable sets}

\begin{center}
$\ENU = \{V_P \suchthat P\in \PREDREC\}$
\end{center}
\begin{remark}
$\DEC\subset\ENU$ since $\PREDTREC\subset\PREDREC$.
\end{remark}
\end{definition}

\begin{definition}{Recursively generable set}

$V$ is a recursively generable set iff $V$ is recursively enumerable.
\end{definition}
