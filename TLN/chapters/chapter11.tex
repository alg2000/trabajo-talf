	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHAPTER 11: Universality
\chapter{Universality}
\localtableofcontents
\newpage

\section{Program numbering}

\begin{definition}{Index of a set of functions}

An index of a countable set of functions $\textbf{F}$ is any surjective function $h: \mathbb{N} \to \textbf{F}$

An index of a function $f \in \textbf{F}$ under the index $h$ is any $i \in \mathbb{N}$ such that $h(i)=f$.

\end{definition}

In order to codify programs, we need to introduce the concepts of Gödel's encoding and decoding. These functions are defined using the set of all vectors of natural numbers.
\begin{center}
$\mathbb{N}^*=\bigcup\limits_{n=0}^{\infty} \mathbb{N}^n$
\end{center}

\begin{remark}
$\mathbb{N}^0=\{(\,)\}$.
\end{remark}

\newpage


%% Cantor %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{definition}{Cantor pairing function \translation{cantorización de $\mathbb{N}^2$}}

Cantor pairing establishes a bijection between $\mathbb{N}$ and $\mathbb{N}^2$.

\begin{center}
$\begin{aligned}
&\sigma^2_1 \;:\; \mathbb{N}\times\mathbb{N} \to \mathbb{N}\\
&\sigma^2_1(x,y) = \frac{(x+y)(x+y+1)}{2}+y\\ \\
&\sigma^1_{2,2} \;:\; \mathbb{N} \to \mathbb{N}\times\mathbb{N}\\
&\sigma^1_{2,2}(z) = z - \sigma^2_1(d(z), 0)\\ \\
&\sigma^1_{2,1} \;:\; \mathbb{N} \to \mathbb{N}\times\mathbb{N}\\
&\sigma^1_{2,1}(z) = d(z) - \sigma^1_{2,2}(z)
\end{aligned}$
\end{center}
where
\begin{center}
$d(z) = min\{ t \in \mathbb{N} \suchthat \sigma^2_1(t,0) > z \} - 1$
\end{center}

(See \href{https://upload.wikimedia.org/wikipedia/commons/6/6f/Pairing_natural.svg}{figure} in Cantor pairing function Wikipedia article.)

\end{definition}

\newpage


\begin{definition}{Cantor tuple function \translation{cantorización de $\mathbb{N}^{k+1}$}}

Each Cantor tuple function establishes a biyective function between $\mathbb{N}$ and $\mathbb{N}^{k+1}, k \in \mathbb{N}_0$.

\begin{center}
$\begin{aligned}
&\sigma^{k+1}_1 \;:\; \mathbb{N}^{k+1} \to \mathbb{N}\\
&\sigma^1_1(x) = x\\
&\sigma^{k+1}_1(x_1,\dots,x_{k+1}) = \sigma^2_1\left(\sigma^k_1(x_1,\dots,x_{k}),x_{k+1}\right)\\ \\
&\sigma^1_{k+1,p} \;:\; \mathbb{N} \to \mathbb{N}\\
&\sigma^1_{k+1,p}(z) = 
\begin{cases}
 \sigma^1_{k,p}\bigl(\sigma^1_{2,1}(z)\bigr) & \text{if } 1 \leq p \leq k\\\\
 \sigma^1_{2,2}(z)                           & \text{if } p = k+1
\end{cases}
\end{aligned}$
\end{center}
\begin{remark}
Cantor tuple is a family of countable infinite bijective functions.
\end{remark}

\end{definition}

\newpage


%% Godel %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{definition}{Gödel encoding of $\mathbb{N}^*$ \translation{godelización de $\mathbb{N}^*$}}

Gödel encoding establishes a biyection between $\mathbb{N}$ and $\mathbb{N}^* = \bigcup\limits_{k \in \mathbb{N}} \mathbb{N}^k$.
\begin{center}
$\begin{aligned}
&\Gamma \;:\; \mathbb{N}^* \to \mathbb{N}\\
&\Gamma(\vec{x}) = 
\begin{cases}
 0                                                                               & \text{if } \card{\vec{x}} = 0\\
 \sigma^2_1 \left(\card{\vec{x}}-1, \sigma^{\card{\vec{x}}}_1(\vec{x}) \right)+1 & \text{if } \card{\vec{x}} > 0
\end{cases}\\ \\
&\gamma \;:\; \mathbb{N} \to \mathbb{N}^*\\
&\gamma(z,k) = 
\begin{cases}
 0                                                                  & \text{if } z = 0 \lor k > length(z)\\
 \sigma^1_{length(z), k} \left(\sigma^1_{2, 2} (z-1) \right)& \text{otherwise}
\end{cases}
\end{aligned}$
\end{center}
where $length(z)$ refers to the length of the vector obtained from decoding $z$, namely
\begin{center}
$length(z)=m $ iff $ \Gamma(\vec{x})=z\ \land \; \vec{x} \in \mathbb{N}^m$
\end{center}
or, in terms of the previous encoding: $length(z) = \sigma^1_{2, 1} (z-1) + 1$ if $z>0$, and $length(0)=0$.\\

\begin{remark}
Gödel encoding enumerates $N^*$:
\begin{center}
\begin{tabular}{ c|c|c|c|c|c|c|c|c|c|c|c }
$0$  & $1$   & $2$      & $3$   & $4$         & $5$      & $6$   & $7$            & $8$         & $9$      & $10$  & $\cdots$ \\
$()$ & $(0)$ & $(0, 0)$ & $(1)$ & $(0, 0, 0)$ & $(1, 0)$ & $(2)$ & $(0, 0, 0, 0)$ & $(1, 0, 0)$ & $(0, 1)$ & $(3)$ & $\cdots$
\end{tabular}
\end{center}
\end{remark}

\end{definition}

\newpage


%% WHILE → ℕ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Encoding of While programs}

\begin{definition}{Encoding of While sentences}\\

$sent2N:\ \CODWHILE^1 \to \mathbb{N} \text{ with } \CODWHILE^1 = \Lb\bigl((N,T,P,<\text{sentence}>)\bigr)$\\
\item $sent2N(c)=\left\{
  \begin{array}{ll}
    5(i-1)                                    & \text{if } c= \zerosentence{\X{i}}             \\
    \\ 5\sigma_1^2(i-1,j-1)+1                 & \text{if } c= \copysentence{\X{i}}{\X{j}}      \\
    \\ 5\sigma_1^2(i-1,j-1)+2                 & \text{if } c= \incrsentence{\X{i}}{\X{j}}      \\
    \\ 5\sigma_1^2(i-1,j-1)+3                 & \text{if } c= \decrsentence{\X{i}}{\X{j}}      \\
    \\ 5\sigma_1^2\bigl(i-1,code2N(b)\bigl)+4 & \text{if } c= \inlinewhilesentence{\X{i}}{$b$} \\
  \end{array} \right.$
\end{definition}

% possible new format, discarding sent2N
%$code2N:\ \CODWHILE \to \mathbb{N}$\\
%\item $code2N(c)=\left\{
  %\begin{array}{ll}
    %5(i-1)                                    & \text{if } c= \zerosentence{\X{i}}             \\
    %\\ 5\sigma_1^2(i-1,j-1)+1                 & \text{if } c= \copysentence{\X{i}}{\X{j}}      \\
    %\\ 5\sigma_1^2(i-1,j-1)+2                 & \text{if } c= \incrsentence{\X{i}}{\X{j}}      \\
    %\\ 5\sigma_1^2(i-1,j-1)+3                 & \text{if } c= \decrsentence{\X{i}}{\X{j}}      \\
    %\\ 5\sigma_1^2\bigl(i-1,code2N(b)\bigl)+4 & \text{if } c= \inlinewhilesentence{\X{i}}{$b$} \\
    %\\ \Gamma\bigl(code2N(s),code2N(b)\bigr)  & \text{if } c= s\text{;\ }b \ \land \ s \in \CODWHILE^1 \ \land \ b \in \CODWHILE \\
  %\end{array} \right.$
%\end{definition}


\newpage


\begin{definition}{Encoding of While codes}

Given $c= s_1; \ldots ;s_m \in \CODWHILE$, where each $s_i$ is a sentence,
\begin{center}
$code2N(c)=\Gamma\bigl(sent2N(s_1),\dots,sent2N(s_m)\bigl)-1$
\end{center}
\end{definition}


\begin{definition}{Encoding of While programs}

Given $Q=(n,c) \in \WHILE$, \\

$while2N: \WHILE \to \mathbb{N}$\\
$while2N(Q) = \sigma^2_1\bigl(n, code2N(c)\bigl) $

\end{definition}

\newpage

\begin{example}{Encoding of the double program}

\whileprogram*{double}{1}{s}{
  \copysentence{\X{2}}{\X{1}}
  \whilesentence{\X{2}}{
    \incrsentence{\X{1}}{\X{1}}
    \decrsentence*{\X{2}}{\X{2}}
  }
}

$\begin{aligned}
&code2N(s)       \approx \num{1e39}\\
&while2N(double) \approx \num{5e77}
\end{aligned}$

\end{example}

\newpage


%% ℕ → WHILE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Decoding While programs}
\begin{definition}{Type of sentence function}

$senttype:\mathbb{N} \to \{0,1,2,3,4\}$ \\
$senttype(z) = z \ mod \ 5$
\end{definition}


\begin{definition}{$LHS$ and $RHS$ functions}

$LHS, RHS:\mathbb{N} \to \mathbb{N}$ \\\\
$LHS(z)= 
\begin{cases}
  1 + \frac{1}{5}z                                          & \text{if } senttype(z)=0 \\
  1 + \sigma^1_{2,1}\left(\frac{1}{5}(z-senttype(z))\right) & \text{if } senttype(z) \in \{1,2,3,4\}
\end{cases}$\\\\\\
$RHS(z)= 1 + \sigma^1_{2,2}\left(\frac{1}{5}(z-senttype(z))\right)$

\end{definition}

\newpage


\begin{definition}{Decoding a While sentence}

$N2sent:\mathbb{N} \to \CODWHILE$ \\
 \item $N2sent(z)=\left\{ \begin{array}{lc}
      \zerosentence{\X{i}}                       & \text{if } senttype(z)=0 \\
   \\ \copysentence{\X{i}}{\X{j}}                & \text{if } senttype(z)=1 \\
   \\ \incrsentence{\X{i}}{\X{j}}                & \text{if } senttype(z)=2 \\
   \\ \decrsentence{\X{i}}{\X{j}}                & \text{if } senttype(z)=3 \\
   \\ \inlinewhilesentence{\X{i}}{$N2code(j-1)$} & \text{if } senttype(z)=4 \\
   \end{array}
 \right.$\\\\\\
where $ i = LHS(z),\ j=RHS(z)$
\end{definition}

\newpage


\begin{definition}{Decoding While codes}

\noindent $N2code:\mathbb{N} \to \CODWHILE$ \\\\
$N2code(z)=
\begin{cases}
  N2sent\bigl(\gamma(z+1,1)\bigl)                                                  & \text{if } z=0 \\
  N2sent\bigl(\gamma(z+1,1)\bigl); \dots ; N2sent\bigl(\gamma(z+1,length(z))\bigl) & \text{if } z>0
\end{cases}$\\\\


\end{definition}


\begin{definition}{Decoding $\WHILE$ programs}

$N2while : \mathbb{N} \to \WHILE$\\
$N2while(z)=\bigl(\sigma^1_{2,1}(z), N2code(\sigma^1_{2,2}(z)) \bigl)$

\end{definition}

\newpage


%% indexing REC %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{corollary}{Indexing of $\REC$}

$h : \mathbb{N} \to F(\WHILE)$ \\
$h(z)= F_{N2while(z)}$
\newline
\newline
Given the equivalence theorem, the function is also an indexing of $\REC$.
\end{corollary}

\begin{corollary}{Indexing of $\REC^n$}

$h : \mathbb{N} \to \FWHILEN{n}$ \\
$h(z)=F_{(n,\;N2code(z))}$
\newline
\newline
Given the equivalence theorem, the function is also an indexing of $\REC^n$.
\end{corollary}

\newpage


%% universal function %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Universal function}
\begin{definition}{Universal function}

Given a countable set of functions $\textbf{F}$ verifying $f \in \textbf{F} \Rightarrow f:\mathbb{N}^n \to \mathbb{N}$, $n\ge0$,  \\\\
$U[\textbf{F}]:\mathbb{N}^{n+1} \to \mathbb{N}$ is universal for $\textbf{F}$ iff
\begin{center}
$\exists \ \noindent h: \mathbb{N} \to \textbf{F}$ that is an indexing of $\textbf{F} \suchthat U[\textbf{F}](i,\underline{x}) = h(i)(\underline{x}) \  \forall i \in \mathbb{N} \ \wedge \ \forall \underline{x} \in \mathbb{N}^n$.
\end{center}
\begin{remark}
$U[\textbf{F}]$ is the universal function for $\textbf{F}$ under the indexing $h$. 
\end{remark}

\end{definition}

\newpage


\begin{theorem} $U[\REC^n] \in \ \REC^{n+1}$

Assuming
\begin{center}
$\begin{aligned}
&\exists \ U \in \WHILE \ \suchthat \ F_U = U[\REC^n] \Rightarrow\\
&U[\REC^n] \in \FWHILEN{n+1} \Rightarrow \\
&U[\REC^n] \in \REC^{n+1}
\end{aligned}$
\end{center}
it has to be demonstrated that $\exists \ U \in \WHILE \ \suchthat \ F_U = U[\REC^n]$
\begin{remark}
$U$ will be denoted as the universal program.
\end{remark}

\end{theorem}

\newpage


\section{Universal program}

\begin{definition}{Universal program}

\whileprogram*{U}{k+1}{s}{
  \Comment{\X{1} stores the code of the program}
  \Comment{\X{2},\dots,\X{k+1} store the arguments of the program}
  \Comment{arguments are encoded in a single Gödel number}
  \exprsentence{\X{k+2}}{godelk(\X{2},\dots,\X{k+1})}
  \Comment{variables after simulating the program}
  \exprsentence{\X{k+2}}{simulate(\X{1},\X{k+2})}
  \Comment{first variable is the output}
  \exprsentence*{\X{1}}{degodel(\X{k+2},1)}
}

\newpage


\whileprogram*{simulate}{2}{s}{
  \Comment{\X{1} = sentence, \X{2} = variables}
  \incrsentence{\X{1}}{\X{1}}
  \whilesentence{\X{1}}{
    \Comment{extract first sentence and check type}
    \exprsentence{\X{3}}{degodel(\X{1},1)}
    \Comment{check if assignment or loop}
    \uIf{\text{\codesetfont senttype(\X{3}) $\leq$ 3}}{
      \Comment{change variable and remove}
      \exprsentence{\X{2}}{execute(\X{3},\X{2})}
      \exprsentence*{\X{1}}{pop(\X{1})}
    }\Else{
      \Comment{value of control variable}
      \exprsentence{\X{4}}{lhs(\X{3})}
      \uIf{\text{\codesetfont degodel(\X{2},\X{4}) $\neq$ 0}}{
        \Comment{push body of the loop to top of the stack}
        \exprsentence{\X{4}}{rhs(\X{3})}
        \exprsentence*{\X{1}}{merge(\X{4})}
      }\Else{
        \Comment{remove sentence from the stack}
        \exprsentence*{\X{1}}{pop(\X{1})}
      }
    }
  }
}

\newpage


\whileprogram{execute}{2}{s}{
  \Comment{\X{1} = sentence, \X{2} = variables}
  \exprsentence{\X{3}}{senttype(\X{1})}
  \Comment{index of target variable}
  \exprsentence{\X{4}}{lhs(\X{1})}
  \uIf{\X{3} = 0}{
    \exprsentence*{\X{1}}{replace(\X{2},\X{4},0)}
  }\Else{
    \Comment{value of source variable}
    \exprsentence{\X{5}}{rhs(\X{1})}
    \exprsentence{\X{5}}{degodel(\X{2},\X{5})}
    \If{\X{3} = 1}{
      \exprsentence*{\X{1}}{replace(\X{2},\X{4},\X{5})}
    }
    \If{\X{3} = 2}{
      \incrsentence{\X{5}}{\X{5}}
      \exprsentence*{\X{1}}{replace(\X{2},\X{4},\X{5})}
    }
    \If{\X{3} = 3}{
      \decrsentence{\X{5}}{\X{5}}
      \exprsentence*{\X{1}}{replace(\X{2},\X{4},\X{5})}
    }
  }
}

\end{definition}

