\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Preamble to discrete mathematics}{9}{chapter.1}% 
\etoc@startlocaltoc {2}
\contentsline {section}{\numberline {1.1}Reminder}{10}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Finite and Infinite Sets}{19}{section.1.2}% 
\contentsline {chapter}{\numberline {2}Languages and grammars}{23}{chapter.2}% 
\etoc@startlocaltoc {3}
\contentsline {section}{\numberline {2.1}Languages}{24}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Concept of language}{24}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Language representation}{31}{subsection.2.1.2}% 
\contentsline {section}{\numberline {2.2}Grammatical representation systems}{32}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Definition and functioning of grammars}{32}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Classification of rules}{35}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Classification of grammars}{37}{subsection.2.2.3}% 
\contentsline {subsection}{\numberline {2.2.4}Classification of languages}{38}{subsection.2.2.4}% 
\contentsline {section}{\numberline {2.3}Questions about languages}{39}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Operations and closures of languages}{40}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Closures for the types of languages}{43}{section.2.5}% 
\contentsline {chapter}{\numberline {3}Regular expressions}{45}{chapter.3}% 
\etoc@startlocaltoc {4}
\contentsline {section}{\numberline {3.1}Definition of the regular expressions}{46}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Some properties of regular expressions}{50}{section.3.2}% 
\contentsline {chapter}{\numberline {4}Finite automata}{53}{chapter.4}% 
\etoc@startlocaltoc {5}
\contentsline {section}{\numberline {4.1}Deterministic finite automata}{54}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Nondeterministic finite automata }{60}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Minimum deterministic finite automaton}{66}{section.4.3}% 
\contentsline {section}{\numberline {4.4}Equivalences}{66}{section.4.4}% 
\contentsline {chapter}{\numberline {5}Regularity conditions}{67}{chapter.5}% 
\etoc@startlocaltoc {6}
\contentsline {section}{\numberline {5.1}Myhill-Nerode}{68}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Pumping lemma}{72}{section.5.2}% 
\contentsline {chapter}{\numberline {6}Context-free languages}{75}{chapter.6}% 
\etoc@startlocaltoc {7}
\contentsline {section}{\numberline {6.1}Derivations and Ambiguity}{76}{section.6.1}% 
\contentsline {section}{\numberline {6.2}Recursion}{81}{section.6.2}% 
\contentsline {section}{\numberline {6.3}CFG Simplification}{83}{section.6.3}% 
\contentsline {section}{\numberline {6.4}Normal forms}{85}{section.6.4}% 
\contentsline {section}{\numberline {6.5}Closure properties}{85}{section.6.5}% 
\contentsline {section}{\numberline {6.6}Non-Deterministic Pushdown Automata}{86}{section.6.6}% 
\contentsline {section}{\numberline {6.7}Pumping}{92}{section.6.7}% 
\contentsline {chapter}{\numberline {7}The Turing machine}{93}{chapter.7}% 
\etoc@startlocaltoc {8}
\contentsline {section}{\numberline {7.1}Formal definition}{94}{section.7.1}% 
\contentsline {section}{\numberline {7.2}Computability, decidability, enumerability}{105}{section.7.2}% 
\contentsline {chapter}{\numberline {8}Recursive functions}{108}{chapter.8}% 
\etoc@startlocaltoc {9}
\contentsline {section}{\numberline {8.1}Formal definition}{109}{section.8.1}% 
\contentsline {section}{\numberline {8.2}Enumerability, decidability, computability}{117}{section.8.2}% 
\contentsline {chapter}{\numberline {9}The WHILE language}{119}{chapter.9}% 
\etoc@startlocaltoc {10}
\contentsline {section}{\numberline {9.1}Formal definition}{120}{section.9.1}% 
\contentsline {section}{\numberline {9.2}Computability, decidability, enumerability}{127}{section.9.2}% 
\contentsline {chapter}{\numberline {10}Turing completeness}{129}{chapter.10}% 
\contentsline {section}{\numberline {10.1}Extended While language}{130}{section.10.1}% 
\contentsline {section}{\numberline {10.2}Recursive $\Rightarrow $ While-computable}{132}{section.10.2}% 
\contentsline {chapter}{\numberline {11}Universality}{137}{chapter.11}% 
\etoc@startlocaltoc {11}
\contentsline {section}{\numberline {11.1}Program numbering}{138}{section.11.1}% 
\contentsline {subsection}{\numberline {11.1.1}Encoding of While programs}{142}{subsection.11.1.1}% 
\contentsline {subsection}{\numberline {11.1.2}Decoding While programs}{145}{subsection.11.1.2}% 
\contentsline {section}{\numberline {11.2}Universal function}{149}{section.11.2}% 
\contentsline {section}{\numberline {11.3}Universal program}{151}{section.11.3}% 
\contentsline {chapter}{\numberline {12}Theoretical limits of computing}{154}{chapter.12}% 
\etoc@startlocaltoc {12}
\contentsline {section}{\numberline {12.1}Halting problem}{155}{section.12.1}% 
\contentsline {section}{\numberline {12.2}The busy beaver function}{158}{section.12.2}% 
\contentsline {chapter}{References}{161}{chapter*.21}% 
